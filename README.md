## TRTDerivationCheck

This is a simple repository used to check the contents of an ATLAS TRT
Derivation.

This README assumes you're on an `lxplus` like environment

Please email [ddavis@cern.ch](ddavis@cern.ch) with any problems.

## Setup


Setup access to ATLAS tools, setup git, clone the repository, and run
the `setup.sh` script.

```
$ setupATLAS
$ lsetup git
$ git clone --recurse-submodules https://gitlab.cern.ch/atlas-trt-software/TRTDerivationCheck.git
$ cd TRTDerivationCheck
$ source setup.sh
```

Now you have two scripts you can use.

- `checkxAOD.py`: this is an old ATLAS ASG script to check what kind
  of containers are in the derivation. (high level information)
- `checkForVariables.py`: this is a script to actually look at the
  variables you'd have access to when analyzing this derivation.

### Getting a sample to check

To check the contents of a dataset, I'd suggest downloading a single
file locally with `rucio`. Then you can point the scripts to that
file.

```
$ lsetup rucio
$ voms-proxy-init -voms atlas --valid 96:00
$ rucio download --nrandom 1 <dataset name>
```

(If the file is big and you don't want to keep it around for another
reason, it's probably best to clean up after you're done checking it
and just delete the file.)

### Running checkxAOD.py

All you need to do is point the script to your file:

```
$ checkxAOD.py <file>
```

### Running checkForVariables.py

`checkForVariables.py` runs a little deeper and you have some
options. By default, if you point the script to a file, it will look
for the following strings:

- `InDetTrack`
- `TRT`
- `MSOS`
- `DriftCircle`

For example:

```
$ checkForVariables.py -i <file>
```

And print all variables containing those strings. If you want a little
more control, you can use the `-s` option. If you want to look
specifically for variables containing "dEdx" or "tot" or "ToT", you
would run (case sensitive):

```
$ checkForVariables.py -i <file> -s dEdx tot ToT
```

### checkxAOD.py output

From `checkxAOD.py` on data; you'll see something like this (see that triggers are available in this sample):
```
========================================================================================================================
 File: ../group.det-indet.data17_13TeV.00340453.physics_Main.daq.TRT_xAOD_Zee.f894_EXT0/group.det-indet.13118507.EXT0._001199.InDetDxAOD.pool.root
------------------------------------------------------------------------------------------------------------------------
    Memory size        Disk Size       Size/Event  Compression Entries  Name (Type)
------------------------------------------------------------------------------------------------------------------------
         1.57 kB          1.02 kB     0.13 kB/event    1.53         8   MBTSContainer (TileCellVec) [Calo]
         6.62 kB          1.56 kB     0.20 kB/event    4.24         8   ByteStreamEventInfo (EventInfo_p4) [EvtId]
         6.43 kB          1.69 kB     0.21 kB/event    3.81         8   BCM_RDOs (BCM_RDO_Container_p0) [InDet]
        88.24 kB          8.18 kB     1.02 kB/event   10.78         8   xTrigDecision (xAOD::TrigDecision_v1) [Trig]
        34.56 kB         19.82 kB     2.48 kB/event    1.74         8   EventInfo (xAOD::EventInfo_v1) [EvtId]
       163.65 kB         41.95 kB     5.24 kB/event    3.90         8   PrimaryVertices (DataVector<xAOD::Vertex_v1>) [InDet]
        84.77 kB         66.49 kB     8.31 kB/event    1.27         8   Electrons (DataVector<xAOD::Electron_v1>) [egamma]
        89.91 kB         68.02 kB     8.50 kB/event    1.32         8   Muons (DataVector<xAOD::Muon_v1>) [Muon]
       134.27 kB         86.57 kB    10.82 kB/event    1.55         8   GSFTrackParticles (DataVector<xAOD::TrackParticle_v1>) [egamma]
       355.86 kB         97.85 kB    12.23 kB/event    3.64         8   TrigNavigation (xAOD::TrigNavigation_v1) [Trig]
       361.80 kB        102.03 kB    12.75 kB/event    3.55         8   HLTResult_HLT (HLT::HLTResult_p1) [Trig]
      9869.66 kB       2001.97 kB   250.25 kB/event    4.93         8   InDetTrackParticles (DataVector<xAOD::TrackParticle_v1>) [InDet]
     26005.80 kB       7216.30 kB   902.04 kB/event    3.60         8   TRT_DriftCircles (DataVector<xAOD::TrackMeasurementValidation_v1>) [InDet]
     24273.45 kB      12659.65 kB  1582.46 kB/event    1.92         8   TRT_MSOSs (DataVector<xAOD::TrackStateValidation_v1>) [InDet]
------------------------------------------------------------------------------------------------------------------------
     61476.60 kB      22373.11 kB  2796.64 kB/event                     Total
========================================================================================================================

================================================================================
         Categorized data
================================================================================
     Disk Size         Fraction    Category Name
--------------------------------------------------------------------------------
       0.128 kb        0.000       Calo
       2.673 kb        0.001       EvtId
       8.502 kb        0.003       Muon
      19.132 kb        0.007       egamma
      26.008 kb        0.009       Trig
    2740.195 kb        0.980       InDet
    2796.638 kb        1.000       Total

================================================================================
CSV for categories disk size/evt and fraction:
Total,InDet,Trig,egamma,Muon,EvtId,Calo
2796.638,2740.195,26.008,19.132,8.502,2.673,0.128
1.000,0.980,0.009,0.007,0.003,0.001,0.000
================================================================================
```

From an MC samples...
```
========================================================================================================================
 File: ../group.det-indet.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.simul.TRTxAOD.e3601_s2876_r7886_trt099-02_EXT0/group.det-indet.12179430.EXT0._018451.InDetDxAOD.pool.root
------------------------------------------------------------------------------------------------------------------------
    Memory size        Disk Size       Size/Event  Compression Entries  Name (Type)
------------------------------------------------------------------------------------------------------------------------
        22.58 kB         18.01 kB     0.04 kB/event    1.25       500   egammaTruthParticles (DataVector<xAOD::TruthParticle_v1>) [egamma]
        22.34 kB         18.32 kB     0.04 kB/event    1.22       500   MuonTruthParticles (DataVector<xAOD::TruthParticle_v1>) [Muon]
       130.12 kB         30.71 kB     0.06 kB/event    4.24       500   MBTSContainer (TileCellVec) [Calo]
       139.84 kB         38.12 kB     0.08 kB/event    3.67       500   McEventInfo (PileUpEventInfo_p5) [EvtId]
       151.98 kB         46.12 kB     0.09 kB/event    3.30       500   BCM_RDOs (BCM_RDO_Container_p0) [InDet]
      7518.79 kB       1972.67 kB     3.95 kB/event    3.81       500   PrimaryVertices (DataVector<xAOD::Vertex_v1>) [InDet]
      3440.89 kB       2993.08 kB     5.99 kB/event    1.15       500   Electrons (DataVector<xAOD::Electron_v1>) [egamma]
      4089.46 kB       3020.09 kB     6.04 kB/event    1.35       500   GSFTrackParticles (DataVector<xAOD::TrackParticle_v1>) [egamma]
     37393.07 kB       3209.30 kB     6.42 kB/event   11.65       500   TruthEvents (DataVector<xAOD::TruthEvent_v1>) [Truth]
      4709.24 kB       3991.30 kB     7.98 kB/event    1.18       500   Muons (DataVector<xAOD::Muon_v1>) [Muon]
     86436.41 kB       7893.70 kB    15.79 kB/event   10.95       500   TruthPileupEvents (DataVector<xAOD::TruthPileupEvent_v1>) [Truth]
     18292.36 kB      18161.75 kB    36.32 kB/event    1.01       500   EventInfo (xAOD::EventInfo_v1) [EvtId]
    143223.85 kB      25313.43 kB    50.63 kB/event    5.66       500   TruthVertices (DataVector<xAOD::TruthVertex_v1>) [Truth]
    293921.04 kB      60771.72 kB   121.54 kB/event    4.84       500   InDetTrackParticles (DataVector<xAOD::TrackParticle_v1>) [InDet]
    270114.94 kB      71693.83 kB   143.39 kB/event    3.77       500   TruthParticles (DataVector<xAOD::TruthParticle_v1>) [Truth]
    837565.36 kB     206766.21 kB   413.53 kB/event    4.05       500   TRT_DriftCircles (DataVector<xAOD::TrackMeasurementValidation_v1>) [InDet]
    704128.35 kB     375081.64 kB   750.16 kB/event    1.88       500   TRT_MSOSs (DataVector<xAOD::TrackStateValidation_v1>) [InDet]
------------------------------------------------------------------------------------------------------------------------
   2411300.62 kB     781020.02 kB  1562.04 kB/event                     Total
========================================================================================================================

================================================================================
         Categorized data
================================================================================
     Disk Size         Fraction    Category Name
--------------------------------------------------------------------------------
       0.061 kb        0.000       Calo
       8.019 kb        0.005       Muon
      12.062 kb        0.008       egamma
      36.400 kb        0.023       EvtId
     216.221 kb        0.138       Truth
    1289.277 kb        0.825       InDet
    1562.040 kb        1.000       Total

================================================================================
CSV for categories disk size/evt and fraction:
Total,InDet,Truth,EvtId,egamma,Muon,Calo
1562.040,1289.277,216.221,36.400,12.062,8.019,0.061
1.000,0.825,0.138,0.023,0.008,0.005,0.000
================================================================================
```
Notice that triggers are not available, but truth information is!

### checkForVariables.py output

On an MC sample, If I run `checkForVariables.py` and ask for _just_
variables containing "DriftCircle", I get everything in the
`TRT_DriftCircle` container:

```
cern lxplus082 ~/TRTDerivationCheck $ checkForVariables.py -i ../group.det-indet.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.simul.TRTxAOD.e3601_s2876_r7886_trt099-02_EXT0/group.det-indet.12179430.EXT0._018451.InDetDxAOD.pool.root -s DriftCircle
TRT_DriftCircles
TRT_DriftCirclesAux.
TRT_DriftCirclesAux.xAOD::AuxContainerBase
TRT_DriftCirclesAux.identifier
TRT_DriftCirclesAux.rdoIdentifierList
TRT_DriftCirclesAux.localX
TRT_DriftCirclesAux.localY
TRT_DriftCirclesAux.localXError
TRT_DriftCirclesAux.localYError
TRT_DriftCirclesAux.localXYCorrelation
TRT_DriftCirclesAux.globalX
TRT_DriftCirclesAux.globalY
TRT_DriftCirclesAux.globalZ
TRT_DriftCirclesAuxDyn.truth_barcode
TRT_DriftCirclesAuxDyn.bec
TRT_DriftCirclesAuxDyn.strawlayer
TRT_DriftCirclesAuxDyn.layer
TRT_DriftCirclesAuxDyn.strawnumber
TRT_DriftCirclesAuxDyn.phi_module
TRT_DriftCirclesAuxDyn.strawphi
TRT_DriftCirclesAuxDyn.TRTboard
TRT_DriftCirclesAuxDyn.TRTchip
TRT_DriftCirclesAuxDyn.drifttime
TRT_DriftCirclesAuxDyn.status
TRT_DriftCirclesAuxDyn.tot
TRT_DriftCirclesAuxDyn.isHT
TRT_DriftCirclesAuxDyn.T0
TRT_DriftCirclesAuxDyn.leadingEdge
TRT_DriftCirclesAuxDyn.driftTimeToTCorrection
TRT_DriftCirclesAuxDyn.driftTimeHTCorrection
TRT_DriftCirclesAuxDyn.highThreshold
TRT_DriftCirclesAuxDyn.bitPattern
TRT_DriftCirclesAuxDyn.gasType
```

## Advanced

This repository contains a project called `uproot` as a git
submodule. This is a python library for seamlessly interacting with
ROOT files. You have a lot of freedom if you use uproot directly with
python. For example; you can list _everything_ in the file like so:

```
$ python
>>> import uproot
>>> tree = uproot.open('<file>')['CollectionTree']
>>> tree.allkeys()
```

Visit the [uproot](https://github.com/scikit-hep/uproot) page for more
documentation.
