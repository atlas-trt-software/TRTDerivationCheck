#!/usr/bin/env python

from __future__ import print_function
import argparse
import uproot

parser = argparse.ArgumentParser(
    description='TRT DAOD Checker'
)

parser.add_argument('-i','--inFile',dest='inFile',required=True,type=str,
                    help='Input File')
parser.add_argument('-s','--specifics',dest='spec',required=False,nargs='+',type=str,
                    help='Specific strings to look for')
args = parser.parse_args()

t = uproot.open(args.inFile)['CollectionTree']
b = t.allkeys()
b = [bb.decode("utf-8") for bb in b]

vlist = ['InDetTrack','TRT','MSOS','DriftCircle']

for bb in b:
    if args.spec:
        if any(word in bb for word in args.spec):
            print(bb)
    else:
        if any(word in bb for word in vlist):
            print(bb)
